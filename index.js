const axios = require('axios')
const { createImage } = require('./createImage')
const { downloadFile } = require('./createImage')
const { featuredImageExists } = require('./createImage')
const { youtubeImageUrlFactory } = require('./createImage')
const fs = require('fs')
require('dotenv').config();

const STRAPI_TARGET_SERVER = process.env.STRAPI_TARGET_SERVER || 'http://localhost:1338'
const STRAPI_IDENTIFIER = process.env.STRAPI_IDENTIFIER || 'gil.fernandes@onepointltd.com'
const STRAPI_PASSWORD = process.env.STRAPI_PASSWORD || 'Sarovar16108'

console.warn("STRAPI_TARGET_SERVER", STRAPI_TARGET_SERVER)
let jwtTokenContainer = {}

const strapiLogin = async () => {
    console.log(`Logging in to strapi on ${STRAPI_TARGET_SERVER}`)
    const { data } = await axios.post(`${STRAPI_TARGET_SERVER}/auth/local`, {
        identifier: STRAPI_IDENTIFIER,
        password: STRAPI_PASSWORD
    })
    return data.jwt
}

const logintoStrapi = async () => {
    const lastUpdate = jwtTokenContainer['lastUpdate']
    const timeMillis = new Date().getTime()
    if (!lastUpdate || timeMillis - lastUpdate > refreshLimit) {
        jwtTokenContainer['jwt'] = await strapiLogin()
        jwtTokenContainer['timeMillis'] = timeMillis
    }
}

const listVideos = async (jwtToken) => {
    const { data } = await axios.get(
        `${STRAPI_TARGET_SERVER}/videos?_start=0&_limit=1`, {
            headers: {
                Authorization:
                    `Bearer ${jwtToken}`,
            },
        })
    return data
}

const OK = 200

const logMessage = (verb, res, youtubeId) => {
    if(res.status === OK) {
        console.info(`${verb}d video with id ${youtubeId}`)
    } else {
        console.warn(`Could not ${verb} video with id ${youtubeId}. Status: ${res.status}`)
    }
}

function requestConfigFactory (jwtToken) {
    return {
        headers: {
            Authorization:
                `Bearer ${jwtToken}`,
        },
    }
}

async function queryExistingVideo (youtubeId, jwtToken) {
    const requestConfig = requestConfigFactory(jwtToken)
    const response = await axios.get(
        `${STRAPI_TARGET_SERVER}/lighthouse/video/youtube_id/${youtubeId}`,
        requestConfig)
    const videosLength = response.data.length
    return { response, videosLength, requestConfig }
}

const shouldDownloadImage = (response, videosLength) => {
    return videosLength === 0 || !response.data[0].featuredImage && response.data[0].published_at === null
}

const downloadAndUploadImage = async (youtubeId, requestConfig, imageName) => {
    const imageExists = await featuredImageExists(imageName, requestConfig)
    if(!imageExists) {
        console.log(`Image does not exist: ${imageName}`)
        return await downloadFile(youtubeImageUrlFactory(youtubeId))
    }
    return null
}

const saveVideo = async (video) => {
    const { logintoStrapi, jwtTokenContainer } = require('../processVideo')
    await logintoStrapi()
    const jwtToken = jwtTokenContainer['jwt']
    const youtubeId = video['youtube_id']
    const { response, videosLength, requestConfig } = await queryExistingVideo(youtubeId, jwtToken)

    let featuredImageId = null
    let localImageFile = null
    if(shouldDownloadImage(response, videosLength)) {
        const imageName =  youtubeImageUrlFactory(youtubeId, false)
        localImageFile = await downloadAndUploadImage(youtubeId, requestConfig)
        console.log(`local image: ${localImageFile}`)
        featuredImageId = await createImage(localImageFile, imageName, requestConfig)
    }
    console.log(`featured image id: ${featuredImageId}`)
    if(!!featuredImageId) {
        video['featuredImage'] = featuredImageId
        if(!!localImageFile) {
            fs.unlinkSync(localImageFile)
        }
    }

    if(videosLength === 0) { // Create
        const res = await axios.post(
            `${STRAPI_TARGET_SERVER}/videos`, video, requestConfig
        )
        logMessage("Create", res, youtubeId)
    } else { // Update
        const firstVideo = response.data[0]
        if(firstVideo.published_at === null) {
            const res = await axios.put(
                `${STRAPI_TARGET_SERVER}/videos/${firstVideo.id}`, video,
                requestConfig)
            logMessage("Update", res, youtubeId)
        } else {
            console.warn("Skipping already published", youtubeId)
        }
    }
}

const changeStatus = async (youtubeId) => {
    const { logintoStrapi, jwtTokenContainer } = require('../processVideo')
    await logintoStrapi()
    const jwtToken = jwtTokenContainer['jwt']
    const { requestConfig, response, videosLength } = await queryExistingVideo(youtubeId, jwtToken)
    if(videosLength > 0 && response.data && response.data.length > 0) {
        // Do nothing
        const data = response.data[0]
        console.log('Changing status on ', data)
        data.status = "proposed"
        const res = await axios.put(`${STRAPI_TARGET_SERVER}/videos/${data.id}`, data, requestConfig)
        logMessage("Change status update result: ", res, youtubeId)
    }
}

module.exports = {
    strapiLogin,
    logintoStrapi,
    jwtTokenContainer,
    saveVideo,
    changeStatus,
    requestConfigFactory,
    STRAPI_TARGET_SERVER
}
