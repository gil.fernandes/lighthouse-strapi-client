const fs = require('fs')
const axios = require('axios').default
const FormData = require('form-data')
const tmp = require('tmp');
const { STRAPI_TARGET_SERVER } = require('./index')

const youtubeImageUrlFactory = (youtubeId, full = true) =>
    full
        ? `https://i.ytimg.com/vi/${youtubeId}/hq720.jpg`
        : `${youtubeId}/hq720.jpg`

function initStrapiServer () {
    if(typeof STRAPI_TARGET_SERVER === "undefined") {
        const { STRAPI_TARGET_SERVER } = require('./index')
        return STRAPI_TARGET_SERVER
    }
    return STRAPI_TARGET_SERVER
}

const imageExists = async (featuredImage, requestHeaders) => {
    const STRAPI_TARGET_SERVER = initStrapiServer()
    try {
        const resp = await axios.get(
            `${STRAPI_TARGET_SERVER}/upload/files?name=${encodeURIComponent(
                featuredImage)}`, { ...requestHeaders })
        if (resp.data.length > 0) {
            return resp.data[0].id
        }
        return null
    } catch (e) {
        if (e?.response?.status === 404) {
            return null
        }
        throw e
    }
}

const downloadFile = async (fileUrl, prefix='lighthouse-strapi-client-', outputLocationPath) => {
    if(!outputLocationPath) {
        outputLocationPath = tmp.fileSync({ mode: 0o644, prefix: prefix, postfix: '.jpg' });
    }
    let path = typeof outputLocationPath === 'object' ? outputLocationPath.name : outputLocationPath
    const writer = fs.createWriteStream(path)
    const response = await axios.get(fileUrl, { responseType: 'arraybuffer' })
    return new Promise((resolve, reject) => {
        if(response.data instanceof Buffer) {
            writer.write(response.data)
            resolve(outputLocationPath.name)
        } else {
            response.data.pipe(writer)
            let error = null
            writer.on('error', err => {
                error = err
                writer.close()
                reject(err)
            })
            writer.on('close', () => {
                if (!error) {
                    resolve(outputLocationPath.name)
                }
            })
        }
    })
}

const createImage = async (featuredImage, fileName, requestConfig) => {
    const formData = new FormData()
    formData.append('files', fs.createReadStream(featuredImage), featuredImage)
    formData.append('fileInfo', JSON.stringify({ name: fileName }))
    try {
        const STRAPI_TARGET_SERVER = initStrapiServer()
        const allHeaders = { ...formData.getHeaders(), ...requestConfig['headers'] }
        let featuredImageResp = await axios.post(
            `${STRAPI_TARGET_SERVER}/upload`, formData,
            { headers: allHeaders })
        return featuredImageResp.data[0]['id']
    } catch (e) {
        console.error('Could not upload image', e.response)
    }
    return null
}

const updateImage = async (imageId, fileInfo, requestConfig) => {
    const STRAPI_TARGET_SERVER = initStrapiServer()
    try {
        const targetUrl = `${STRAPI_TARGET_SERVER}/upload/?id=${imageId}`
        const formData = new FormData();
        formData.append('fileInfo', JSON.stringify(fileInfo))
        const allHeaders = { ...formData.getHeaders(), ...requestConfig['headers'] }
        const response = await axios.post(targetUrl, formData, { headers: allHeaders })
        return response.data
    } catch(e) {
        console.error('Could not delete image', e.response)
    }
    return null
}

const deleteImage = async (imageId, requestConfig) => {
    const STRAPI_TARGET_SERVER = initStrapiServer()
    try {
        const targetUrl = `${STRAPI_TARGET_SERVER}/upload/files/${imageId}`
        const response = await axios.delete(targetUrl, requestConfig)
        return response.data
    } catch(e) {
        console.error('Could not delete image', e.response)
    }
    return null
}

module.exports = {
    youtubeImageUrlFactory,
    featuredImageExists: imageExists,
    downloadFile,
    createImage,
    deleteImage,
    updateImage
}