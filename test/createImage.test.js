const { downloadFile } = require('../createImage')
const { requestConfigFactory } = require('../index')
const { featuredImageExists } = require('../createImage')
const fs = require('fs')
const { updateImage } = require('../createImage')
const { deleteImage } = require('../createImage')
const { createImage } = require('../createImage')

it('when featuredImageExists should not exist', () => {
    const { logintoStrapi, jwtTokenContainer } = require('../index')
    let jwtToken
    return logintoStrapi().then(() => {
        return new Promise((resolve) => resolve(jwtTokenContainer['jwt']))
    }).then((jwt) => {
        expect(jwt).toBeTruthy()
        return new Promise((resolve) => resolve(requestConfigFactory(jwt)))
    }).then((requestConfig) => {
        return featuredImageExists('asdsadasdaasdasdasd', requestConfig)
    }).then((result) => {
        expect(result).toBe(null)
    })
})

it('when featuredImageExists should exist', () => {
    const { logintoStrapi, jwtTokenContainer } = require('../index')
    let jwtToken
    return logintoStrapi().then(() => {
        return new Promise((resolve) => resolve(jwtTokenContainer['jwt']))
    }).then((jwt) => {
        expect(jwt).toBeTruthy()
        return new Promise((resolve) => resolve(requestConfigFactory(jwt)))
    }).then((requestConfig) => {
        return featuredImageExists('lights-g04b653941_1920.jpg', requestConfig)
    }).then((result) => {
        expect(result).toBeTruthy()
    })
})

it('when downloadFile should downloaded', () => {
    downloadFile('https://i.ytimg.com/vi/HhpbzPMCKDc/hq720.jpg').then((file) => {
        expect(file).toBeTruthy()
        expect(file.length).toBeGreaterThan(10)
        fs.unlinkSync(file)
    })
})

it('when create file delete file', () => {
    const { logintoStrapi, jwtTokenContainer } = require('../index')
    const fileName = 'HhpbzPMCKDc/hq720.jpg'
    let requestConfig = null
    downloadFile(`https://i.ytimg.com/vi/${fileName}`).then((file) => {
        expect(file).toBeTruthy()
        expect(file.length).toBeGreaterThan(10)
        return new Promise((resolve) => {
            logintoStrapi().then(() => {
                resolve([file, jwtTokenContainer['jwt']])
            })
        })
    }).then(([file, jwt]) => {
        return new Promise((resolve) => {
            requestConfig = requestConfigFactory(jwt)
            createImage(file, fileName, requestConfig).then(id => resolve(id))
        })
    }).then(id => {
        return new Promise((resolve) => {
            const fileInfo = {"alternativeText":"Testing", "caption":"Gil", "name":"testname"}
            updateImage(id, fileInfo, requestConfig).then(resp => resolve(id))
        })
    }).then(id => {
        return new Promise((resolve) => {
            deleteImage(id, requestConfig).then(resp => resolve(resp))
        })
    }).then(resp => {
        console.log('resp', resp)
    })
})